#!/usr/bin/env python3

from turtle import Turtle

SIZE = 800
LEVEL_MAX = 6

X = -400
Y = 200

def draw(t, l, n):
    if n<=0:
        pass
    elif n==1:
        t.forward(l)
    else:
        draw(t, l/3, n-1)
        t.left(60)
        draw(t, l/3, n-1)
        t.right(120)
        draw(t, l/3, n-1)
        t.left(60)
        draw(t, l/3, n-1)

t = Turtle()

t.reset()

t.hideturtle()
t.up()
t.goto(X, Y)
t.down()
t.speed("fastest")

level = LEVEL_MAX

draw(t, SIZE, level)
t.right(120)
draw(t, SIZE, level)
t.right(120)
draw(t, SIZE, level)
t.right(120)

t.getscreen().exitonclick()